#!/usr/bin/env python

from pysawsim import sawsim_histogram as sh
from pysawsim.manager.mpi import MPI_worker_death


MPI_worker_death()
sh.main()

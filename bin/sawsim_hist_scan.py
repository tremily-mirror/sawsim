#!/usr/bin/env python

from pysawsim import parameter_scan as ps
from pysawsim.manager.mpi import MPI_worker_death


MPI_worker_death()
ps.main()

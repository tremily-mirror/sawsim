#!/usr/bin/env python

from pysawsim.parameter_error import main
from pysawsim.manager.mpi import MPI_worker_death


MPI_worker_death()
main()

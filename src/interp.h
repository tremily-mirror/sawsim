/* A full-fledged linear interpolating lookup table interface. */

/* the type of functions we'll interpolate for */
typedef double interp_fn_t (double x, void *params);
/* 0: acceptable tolerance, or 1: unacceptable? */
typedef int interp_tol_fn_t(double xA, double yA, double xB, double yB);

typedef struct interp_table {
  void *table;
  interp_fn_t *fn;
  interp_tol_fn_t *tol;
} interp_table_t;

extern interp_table_t *interp_table_allocate (interp_fn_t *fn, interp_tol_fn_t *tol);
extern void interp_table_free (interp_table_t *itable);
extern double interp_table_eval (interp_table_t *itable, void *params, double x);

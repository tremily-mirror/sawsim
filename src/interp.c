/* A full-fledged interpolating lookup table implementation. */

#include <stdlib.h> /* malloc, free */
#include <stdio.h>  /* printf       */
#include <math.h>   /* sin          */
#include <assert.h> /* assert       */
#include "tavl.h"   /* tavl_*       */
#include "interp.h"

typedef struct evaluated_func_struct {
  double x;
  double f_of_x;
} evaluated_func_t;

/* Comparison function for pointers to |evaluated_func_t|s.
   |param| is not used. */
/* adjusted from compare_ints in test.c */
int
compare_evaluated_func_ts (const void *pa, const void *pb, void *param)
{
  const evaluated_func_t *a = pa;
  const evaluated_func_t *b = pb;

  if (a->x < b->x)
    return -1;
  else if (a->x > b->x)
    return +1;
  else
    return 0;
}

void *allocate_evaluated_func_t (double x, double f_of_x)
{
  evaluated_func_t *eft=NULL;
  eft = (evaluated_func_t *)malloc(sizeof(evaluated_func_t));
  assert(eft != NULL);
  eft->x = x;
  eft->f_of_x = f_of_x;
  return eft;
}

void free_evaluated_func_t (void *tavl_item, void *tavl_param)
{
  free(tavl_item);
}


interp_table_t *interp_table_allocate (interp_fn_t *fn, interp_tol_fn_t *tol)
{
  interp_table_t *itable=NULL;
  itable = (interp_table_t *)malloc(sizeof(interp_table_t));
  assert(itable != NULL);
  itable->table = (void *)tavl_create(&compare_evaluated_func_ts, NULL, NULL);
  itable->fn = fn;
  itable->tol = tol;
  return itable;
}

void interp_table_free (interp_table_t *itable)
{
  tavl_destroy((struct tavl_table *)itable->table, &free_evaluated_func_t);
  free(itable);
}

double interp_table_eval (interp_table_t *itable, void *params, double x)
{
  struct tavl_table *table = (struct tavl_table *)itable->table;
  const evaluated_func_t *a, *b, *c;
  double y, weight;

  /* attempt to look up the x variable */
  a = tavl_bracket(table, &x, (const void **)&b, (const void **)&c);
  if (a != NULL) /* direct hit */
    y = a->f_of_x;
  else {
    if (c == NULL || b == NULL) { /* out of interpolation range, run an actual eval */
      y = (*itable->fn)(x, params);
      tavl_insert(table, allocate_evaluated_func_t (x,y) );
    } else {
      //printf("bracketed x=%g with %g, %g\n", x, b->x, c->x);
      /* should we interpolate? */
      if ( (*itable->tol)(b->x, b->f_of_x, c->x, c->f_of_x) == 0 ) {
	/* interpolate */
	weight = (x - b->x) / (c->x - b->x);
	y = b->f_of_x + (c->f_of_x - b->f_of_x)*weight;
	//printf("interpolated f(%g) ~= %g\n", x, y);
	//fprintf(stderr, "%g\t%g\n", x, y);
      } else { /* tolerance exceeded, run an actual eval */
	y = (*itable->fn)(x, params);
	tavl_insert(table, allocate_evaluated_func_t (x,y) );
      }
    }
  }
  return y;
}

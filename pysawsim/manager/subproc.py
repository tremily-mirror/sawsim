# Copyright (C) 2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Functions for running external commands on other hosts.
"""

try:
    from multiprocessing import Manager, Process, Queue, cpu_count
    _ENABLED = True
    _DISABLING_ERROR = None
    _SKIP = ''
except ImportError, _DISABLING_ERROR:
    _ENABLED = False
    Process = object
    _SKIP = '  # doctest: +SKIP'

import os

from .. import log
from . import Job
from .thread import ThreadManager, CLOSE_MESSAGE


if _ENABLED == True:
    """Check succeptibility to python issue 5313:
      http://bugs.python.org/issue5155
      http://bugs.python.org/issue5313
      http://svn.python.org/view?view=rev&revision=73708

    Fix merged into the 2.6 maintenance branch with

      changeset:   531:11e5b7504a71
      branch:      release26-maint
      user:        r.david.murray
      date:        Tue Jul 21 19:02:14 2009 +0200
      summary:     [svn r74145] Merged revisions 73708,73738 via svnmerge...

    Which came between 2.6.2 and 2.6.3rc1

      $ hg blame -r 631 README | grep 'version 2\.6\.'
      631: This is Python version 2.6.3rc1
      $ hg blame -r 630 README | grep 'version 2\.6\.'
      345: This is Python version 2.6.2
    """
    import sys
    _HAS_BUG_5313 = sys.version_info < (2,6,3)


class WorkerProcess (Process):
    def __init__(self, spawn_queue, receive_queue, *args, **kwargs):
        if _ENABLED == False:
            raise _DISABLING_ERROR
        super(WorkerProcess, self).__init__(*args, **kwargs)
        self.spawn_queue = spawn_queue
        self.receive_queue = receive_queue

    def run(self):
        while True:
            msg = self.spawn_queue.get()
            if msg == CLOSE_MESSAGE:
                log().debug('%s closing' % self.name)
                break
            assert isinstance(msg, Job), msg
            log().debug('%s running job %s' % (self.name, msg))
            msg.run()
            self.receive_queue.put(msg)


class SubprocessManager (ThreadManager):
    """Manage asynchronous `Job` execution via :mod:`subprocess`.

    >>> from time import sleep
    >>> from math import sqrt
    >>> m = SubprocessManager()%(skip)s
    >>> group_A = []
    >>> for i in range(10):%(skip)s
    ...     t = max(0, 5-i)
    ...     group_A.append(m.async_invoke(Job(target=sleep, args=[t])))
    >>> group_B = []
    >>> for i in range(10):%(skip)s
    ...     group_B.append(m.async_invoke(Job(target=sqrt, args=[i],
    ...                 blocks_on=[j.id for j in group_A])))
    >>> jobs = m.wait(ids=[j.id for j in group_A[5:8]])%(skip)s
    >>> print sorted(jobs.values(), key=lambda j: j.id)%(skip)s
    [<Job 5>, <Job 6>, <Job 7>]
    >>> jobs = m.wait()%(skip)s
    >>> print sorted(jobs.values(), key=lambda j: j.id)%(skip)s
    ... # doctest: +NORMALIZE_WHITESPACE
    [<Job 0>, <Job 1>, <Job 2>, <Job 3>, <Job 4>, <Job 8>, <Job 9>, <Job 10>,
     <Job 11>, <Job 12>, <Job 13>, <Job 14>, <Job 15>, <Job 16>, <Job 17>,
     <Job 18>, <Job 19>]
    >>> m.teardown()%(skip)s
    """ % {'skip': _SKIP}
    def __init__(self, worker_pool=None):
        if _ENABLED == False:
            raise _DISABLING_ERROR
        super(SubprocessManager, self).__init__(worker_pool=worker_pool)

    def _setup_queues(self):
        self._spawn_queue = Queue()
        self._receive_queue = Queue()

    def _spawn_workers(self, worker_pool=None):
        if worker_pool is None:
            worker_pool = int(os.environ.get('WORKER_POOL', cpu_count() + 1))
        self._manager = Manager()
        self._workers = []
        for i in range(worker_pool):
            worker = WorkerProcess(spawn_queue=self._spawn_queue,
                                   receive_queue=self._receive_queue,
                                   name='worker-%d' % i)
            log().debug('start %s' % worker.name)
            worker.start()
            self._workers.append(worker)

    def _put_job_in_spawn_queue(self, job):
        """Place a job in the spawn queue."""
        self._spawn_queue.put(job)

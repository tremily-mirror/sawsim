# Copyright (C) 2009-2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drudge's University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

import numpy

from . import PYSAWSIM_LOG_LEVEL_MSG as _PYSAWSIM_LOG_LEVEL_MSG
from .histogram import Histogram
from .manager import MANAGERS, get_manager
from .sawsim import SawsimRunner


_multiprocess_can_split_ = True
"""Allow nosetests to split tests between processes.
"""


def sawsim_histogram(sawsim_runner, param_string, N=400, bin_edges=None):
        """Run `N` simulations and return a histogram with `bin_edges`.

        If `bin_edges == None`, return a numpy array of all unfolding
        forces.
        """
        events = []
        for run in sawsim_runner(param_string=param_string, N=N):
            events.extend([event.force for event in run
                           if (event.initial_state == 'folded'
                               and event.final_state == 'unfolded')])
        events = numpy.array(events)
        if bin_edges == None:
            return events
        h = Histogram()
        h.from_data(events, bin_edges)
        return h


def main(argv=None):
    """
    >>> main(['-N', '2'])
    ... # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
    #Force (N)  Unfolding events
    ...
    """
    from optparse import OptionParser
    import sys

    if argv == None:
        argv = sys.argv[1:]

    sr = SawsimRunner()

    usage = '%prog [options]'
    epilog = '\n'.join([
            'Generate an unfolding force histogram from a series of `sawsim`',
	    'runs.',
            _PYSAWSIM_LOG_LEVEL_MSG,
	    ])
    parser = OptionParser(usage, epilog=epilog)
    parser.format_epilog = lambda formatter: epilog+'\n'
    for option in sr.optparse_options:
        parser.add_option(option)
    parser.add_option('-w', '--bin-width', dest='bin_width',
                      metavar='FLOAT', type='float',
                      help='Histogram bin width in newtons (%default).',
                      default=10e-12)

    options,args = parser.parse_args(argv)

    sr_call_params = sr.initialize_from_options(options)
    try:
        events = sawsim_histogram(
            sawsim_runner=sr, bin_edges=None, **sr_call_params)
    finally:
        sr.teardown()

    if options.bin_width == None:
        sys.stdout.write('# Unfolding force (N)\n')
        events.tofile(sys.stdout, sep='\n')
        sys.stdout.write('\n')
    else:
        h = Histogram()
        h.from_data(events, bin_edges=h.calculate_bin_edges(
                events, options.bin_width))
        h.headings = ['Force (N)', 'Unfolding events']
        h.to_stream(sys.stdout)

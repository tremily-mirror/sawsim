# Copyright (C) 2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Wrap `sawsim` to facilitate more complicated analysis.

Testing pysawsim
================

pysawsim's test framework is build using doctest_, unittest_, and nose_.
`nosetests` (from the nose_ package) scans through the source tree,
searching out the various tests and running them.  If you aren't
familiar with nose_, there is excellent documentation on its home
page.  We use nose_ because its auto-discovery allows us to avoid
collecting all of our assorted tests into
:class:`unittest.TestSuite`\s and running them by hand.

To run the test suite from the sawsim installation directory, just use::

    nosetests --with-doctest --doctest-tests pysawsim/

.. _doctest: http://docs.python.org/library/doctest.html
.. _unittest: http://docs.python.org/library/unittest.html
.. _nose: http://somethingaboutorange.com/mrl/projects/nose/0.11.3/


Adding tests to modules
-----------------------

Just go crazy with doctests and unittests; nose_ will find them.
"""

import logging
import logging.handlers
import sys as _sys
import os as _os


_multiprocess_shared_ = True
"""Allow nosetests to share this module between test processes.

This module cannot be split because _log setup is not re-entrant.
"""

__version__ = '0.10'  # match sawsim version


PYSAWSIM_LOG_LEVEL_MSG = """
You can control the log verbosity with the `PYSAWSIM_LOG_LEVEL`
environmental variable.  Set it to one of the level names in Python's
`logging` module (e.g. `DEBUG`).
"""

def log():
    return logging.getLogger('pysawsim')

def add_stderr_log_handler(level=None):
    if level == None:
        level_string = _os.environ.get('PYSAWSIM_LOG_LEVEL', 'WARNING')
        try:
            level = getattr(logging, level_string)
        except AttributeError:
            _sys.stderr.write(
                'unrecognized PYSAWSIM_LOG_LEVEL: %s\n' % level_string)
            raise
    _log = log()
    _log.setLevel(level)
    console = logging.StreamHandler()
    formatter = logging.Formatter('%(name)-8s: %(levelname)-6s %(message)s')
    console.setFormatter(formatter)
    _log.addHandler(console)

add_stderr_log_handler()

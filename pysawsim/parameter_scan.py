# Copyright (C) 2009-2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Experiment vs. simulation comparison and scanning.
"""

from os import getpid  # for rss()
import os.path
import pickle
from StringIO import StringIO

import numpy

from . import log
from . import PYSAWSIM_LOG_LEVEL_MSG as _PYSAWSIM_LOG_LEVEL_MSG
from .histogram import Histogram as _Histogram
from .histogram import FIGURE as _FIGURE
from .sawsim_histogram import sawsim_histogram
from .sawsim import SawsimRunner

# import after .histogram, which selects an appropriate backend
import pylab

_multiprocess_can_split_ = True
"""Allow nosetests to split tests between processes.
"""

EXAMPLE_HISTOGRAM_FILE_CONTENTS = """# Velocity histograms
# Other general comments...

#HISTOGRAM: -v 6e-7
#Force (N)\tUnfolding events
1.4e-10\t1
1.5e-10\t0
1.6e-10\t4
1.7e-10\t6
1.8e-10\t8
1.9e-10\t20
2e-10\t28
2.1e-10\t38
2.2e-10\t72
2.3e-10\t110
2.4e-10\t155
2.5e-10\t247
2.6e-10\t395
2.7e-10\t451
2.8e-10\t430
2.9e-10\t300
3e-10\t116
3.1e-10\t18
3.2e-10\t1

#HISTOGRAM: -v 8e-7
#Force (N)\tUnfolding events
8e-11\t1
9e-11\t0
1e-10\t0
1.1e-10\t1
1.2e-10\t0
1.3e-10\t0
1.4e-10\t0
1.5e-10\t3
1.6e-10\t3
1.7e-10\t4
1.8e-10\t4
1.9e-10\t13
2e-10\t29
2.1e-10\t39
2.2e-10\t60
2.3e-10\t102
2.4e-10\t154
2.5e-10\t262
2.6e-10\t402
2.7e-10\t497
2.8e-10\t541
2.9e-10\t555
3e-10\t325
3.1e-10\t142
3.2e-10\t50
3.3e-10\t13

#HISTOGRAM: -v 1e-6
#Force (N)\tUnfolding events
1.5e-10\t2
1.6e-10\t3
1.7e-10\t7
1.8e-10\t8
1.9e-10\t7
2e-10\t25
2.1e-10\t30
2.2e-10\t58
2.3e-10\t76
2.4e-10\t159
2.5e-10\t216
2.6e-10\t313
2.7e-10\t451
2.8e-10\t568
2.9e-10\t533
3e-10\t416
3.1e-10\t222
3.2e-10\t80
3.3e-10\t24
3.4e-10\t2
"""


MEM_DEBUG = False



def rss():
    """
    For debugging memory usage.

    resident set size, the non-swapped physical memory that a task has
    used (in kilo-bytes).
    """
    call = "ps -o rss= -p %d" % getpid()
    status,stdout,stderr = invoke(call)
    return int(stdout)


class HistogramMatcher (object):
    """Compare experimental histograms to simulated data.

    The main entry points are `fit()` and `plot()`.

    The input `histogram_stream` should contain a series of
    experimental histograms with '#HISTOGRAM: <params>` lines starting
    each histogram.  `<params>` lists the `sawsim` parameters that are
    unique to that experiment.

    >>> from .manager.thread import ThreadManager
    >>> histogram_stream = StringIO(EXAMPLE_HISTOGRAM_FILE_CONTENTS)
    >>> param_format_string = (
    ...     '-s cantilever,hooke,0.05 -N1 '
    ...     '-s folded,null -N8 '
    ...     '-s "unfolded,wlc,{0.39e-9,28e-9}" '
    ...     '-k "folded,unfolded,bell,{%g,%g}" -q folded')
    >>> m = ThreadManager()
    >>> sr = SawsimRunner(manager=m)
    >>> hm = HistogramMatcher(histogram_stream, param_format_string, sr, N=3)
    >>> hm.plot([[1e-5,1e-3,3],[0.1e-9,1e-9,3]], logx=True, logy=False)
    >>> m.teardown()
    """
    def __init__(self, histogram_stream, param_format_string,
                 sawsim_runner, N=400, residual_type='jensen-shannon',
                 plot=False):
        self.experiment_histograms = self._read_force_histograms(
            histogram_stream)
        self.param_format_string = param_format_string
        self.sawsim_runner = sawsim_runner
        self.N = N
        self.residual_type = residual_type
        self._plot = plot

    @staticmethod
    def _read_force_histograms(stream):
        """
        File format:

          # comment and blank lines ignored
          #HISTOGRAM: <histogram-specific params>
          <pysawsim.histogram.Histogram-compatible histogram>
          #HISTOGRAM: <other histogram-specific params>
          <another pysawsim.histogram.Histogram-compatible histogram>
          ...

        >>> import sys
        >>> stream = StringIO(EXAMPLE_HISTOGRAM_FILE_CONTENTS)
        >>> hm = HistogramMatcher(StringIO(), None, None, None)
        >>> histograms = hm._read_force_histograms(stream)
        >>> sorted(histograms.iterkeys())
        ['-v 1e-6', '-v 6e-7', '-v 8e-7']
        >>> histograms['-v 1e-6'].to_stream(sys.stdout)
        ... # doctest: +NORMALIZE_WHITESPACE, +REPORT_UDIFF
        #Force (N)\tUnfolding events
        1.5e-10\t2
        1.6e-10\t3
        1.7e-10\t7
        1.8e-10\t8
        1.9e-10\t7
        2e-10\t25
        2.1e-10\t30
        2.2e-10\t58
        2.3e-10\t76
        2.4e-10\t159
        2.5e-10\t216
        2.6e-10\t313
        2.7e-10\t451
        2.8e-10\t568
        2.9e-10\t533
        3e-10\t416
        3.1e-10\t222
        3.2e-10\t80
        3.3e-10\t24
        3.4e-10\t2
        """
        token = '#HISTOGRAM:'
        hist_blocks = {None: []}
        params = None
        for line in stream.readlines():
            line = line.strip()
            if line.startswith(token):
                params = line[len(token):].strip()
                assert params not in hist_blocks, params
                hist_blocks[params] = []
            else:
                hist_blocks[params].append(line)

        histograms = {}
        for params,block in hist_blocks.iteritems():
            if params == None:
                continue
            h = _Histogram()
            h.from_stream(StringIO('\n'.join(block)))
            histograms[params] = h
        return histograms

    def param_string(self, params, hist_params):
        """Generate a string of options to pass to `sawsim`.
        """
        return '%s %s' % (
            self.param_format_string % tuple(params), hist_params)

    def residual(self, params):
        residual = 0
        for hist_params,experiment_hist in self.experiment_histograms.iteritems():
            sawsim_hist = sawsim_histogram(
                sawsim_runner=self.sawsim_runner,
                param_string=self.param_string(params, hist_params),
                N=self.N, bin_edges=experiment_hist.bin_edges)
            r = experiment_hist.residual(sawsim_hist, type=self.residual_type)
            residual += r
            if self._plot == True:
                title = ", ".join(["%g" % p for p in params]+[hist_params])
                filename = "residual-%s-%g.png" % (
                    title.replace(', ', '_').replace(' ', '_'), r)
                self._plot_residual_comparison(
                    experiment_hist, sawsim_hist, residual=r,
                    title=title, filename=filename)
        log().debug('residual %s: %g' % (params, residual))
        return residual

    def plot(self, param_ranges, logx=False, logy=False, contour=False,
             csv=None):
        if csv:
            csv.write(','.join(('param 1', 'param 2', 'fit quality')) + '\n')
        xranges = param_ranges[0]
        yranges = param_ranges[1]
        if logx == False:
            x = numpy.linspace(*xranges)
        else:
            m,M,n = xranges
            x = numpy.exp(numpy.linspace(numpy.log(m), numpy.log(M), n))
        if logy == False:
            y = numpy.linspace(*yranges)
        else:
            m,M,n = yranges
            y = numpy.exp(numpy.linspace(numpy.log(m), numpy.log(M), n))
        X, Y = pylab.meshgrid(x,y)
        C = numpy.zeros((len(y)-1, len(x)-1))
        for i,xi in enumerate(x[:-1]):
            for j,yj in enumerate(y[:-1]):
                log().info('point %d %d (%d of %d)'
                           % (i, j, i*(len(y)-1) + j, (len(x)-1)*(len(y)-1)))
                params = (xi,yj)
                r = self.residual(params)
                if csv:
                    csv.write(','.join([str(v) for v in (xi,yj,r)]) + '\n')
                C[j,i] = numpy.log(r) # better resolution in valleys
                if MEM_DEBUG == True:
                    log().debug('RSS: %d KB' % rss())
        C = numpy.nan_to_num(C) # NaN -> 0
        fid = file("histogram_matcher-XYC.pkl", "wb")
        pickle.dump([X,Y,C], fid)
        fid.close()
        # read in with
        # import pickle
        # [X,Y,C] = pickle.load(file("histogram_matcher-XYC.pkl", "rb"))
        # ...
        _FIGURE.clear()
        axes = _FIGURE.add_subplot(1, 1, 1)
        if logx == True:
            axes.set_xscale('log')
        if logy == True:
            axes.set_yscale('log')
        if contour == True:
            p = axes.contour(X[:-1,:-1], Y[:-1,:-1], C)
            # [:-1,:-1] to strip dummy last row & column from X&Y.
        else: # pseudocolor plot
            p = axes.pcolor(X, Y, C)
            axes.autoscale_view(tight=True)
        _FIGURE.colorbar(p)
        _FIGURE.savefig("figure.png")

    def _plot_residual_comparison(self, experiment_hist, theory_hist,
                                  residual, title, filename):
        _FIGURE.clear()
        axes = _FIGURE.add_subplot(1, 1, 1)
        axes.hold(True)
        axes.hist(x=experiment_hist.bin_edges[:-1],  # one fake entry for each bin
                  weights=experiment_hist.counts,    # weigh the fake entries by count
                  bins=experiment_hist.bin_edges,
                  align='mid', histtype='stepfilled',
                  color='r')
        axes.hist(x=theory_hist.bin_edges[:-1],  # one fake entry for each bin
                  weights=theory_hist.counts,    # weigh the fake entries by count
                  bins=theory_hist.bin_edges,
                  align='mid', histtype='stepfilled',
                  color='b', alpha=0.5)
        axes.set_title(title)
        _FIGURE.savefig(filename)


def parse_param_ranges_string(string):
    """Parse parameter range stings.

    '[Amin,Amax,Asteps],[Bmin,Bmax,Bsteps],...'
      ->
    [[Amin,Amax,Asteps],[Bmin,Bmax,Bsteps],...]

    >>> parse_param_ranges_string('[1,2,3],[4,5,6]')
    [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]
    >>> parse_param_ranges_string('[1,2,3]')
    [[1.0, 2.0, 3.0]]
    """
    ranges = []
    for range_string in string.split("],["):
        range_number_strings = range_string.strip("[]").split(",")
        ranges.append([float(x) for x in range_number_strings])
    return ranges


def main(argv=None):
    """
    >>> import tempfile
    >>> f = tempfile.NamedTemporaryFile()
    >>> f.write(EXAMPLE_HISTOGRAM_FILE_CONTENTS)
    >>> f.flush()
    >>> main(['-r', '[1e-5,1e-3,3],[0.1e-9,1e-9,3]',
    ...       '-N', '2',
    ...       f.name])
    >>> f.close()
    """
    from optparse import OptionParser
    import sys

    if argv == None:
        argv = sys.argv[1:]

    sr = SawsimRunner()

    usage = '%prog [options] histogram_file'
    epilog = '\n'.join([
            'Compare simulated results against experimental values over a',
            'range of parameters.  Generates a plot of fit quality over',
            'the parameter space.  The histogram file should look something',
            'like:',
            '',
            EXAMPLE_HISTOGRAM_FILE_CONTENTS,
            ''
            '`#HISTOGRAM: <params>` lines start each histogram.  `params`',
            'lists the `sawsim` parameters that are unique to that',
            'experiment.',
            '',
            'Each histogram line is of the format:',
            '',
            '<bin_edge><whitespace><count>',
            '',
            '`<bin_edge>` should mark the left-hand side of the bin, and',
            'all bins should be of equal width (so we know where the last',
            'one ends).',
            _PYSAWSIM_LOG_LEVEL_MSG,
            ])
    parser = OptionParser(usage, epilog=epilog)
    parser.format_epilog = lambda formatter: epilog+'\n'
    for option in sr.optparse_options:
        if option.dest == 'param_string':
            continue
        parser.add_option(option)
    parser.add_option('-f','--param-format', dest='param_format',
                      metavar='FORMAT',
                      help='Convert params to sawsim options (%default).',
                      default=('-s cantilever,hooke,0.05 -N1 -s folded,null -N8 -s "unfolded,wlc,{0.39e-9,28e-9}" -k "folded,unfolded,bell,{%g,%g}" -q folded'))
    parser.add_option('-p','--initial-params', dest='initial_params',
                      metavar='PARAMS',
                      help='Initial params for fitting (%default).',
                      default='3.3e-4,0.25e-9')
    parser.add_option('-r','--param-range', dest='param_range',
                      metavar='PARAMS',
                      help='Param range for plotting (%default).',
                      default='[1e-5,1e-3,20],[0.1e-9,1e-9,20]')
    parser.add_option('--logx', dest='logx',
                      help='Use a log scale for the x range.',
                      default=False, action='store_true')
    parser.add_option('--logy', dest='logy',
                      help='Use a log scale for the y range.',
                      default=False, action='store_true')
    parser.add_option('-R','--residual', dest='residual',
                      metavar='STRING',
                      help='Residual type (from %s; default: %%default).'
                      % ', '.join(_Histogram().types()),
                      default='jensen-shannon')
    parser.add_option('-P','--plot-residuals', dest='plot_residuals',
                      help='Generate residual difference plots for each point in the plot range.',
                      default=False, action='store_true')
    parser.add_option('-c','--contour-plot', dest='contour_plot',
                      help='Select contour plot (vs. the default pseudocolor plot).',
                      default=False, action='store_true')
    parser.add_option('--csv', dest='csv', metavar='FILE',
                      help='Save fit qualities to a comma-separated value file FILE.'),

    options,args = parser.parse_args(argv)

    initial_params = [float(p) for p in options.initial_params.split(",")]
    param_ranges = parse_param_ranges_string(options.param_range)
    histogram_file = args[0]
    csv = None
    sr_call_params = sr.initialize_from_options(options)

    try:
        hm = HistogramMatcher(
            file(histogram_file, 'r'),
            param_format_string=options.param_format,
            sawsim_runner=sr, residual_type=options.residual,
            plot=options.plot_residuals, **sr_call_params)
        #hm.fit(initial_params)
        if options.csv:
            csv = open(options.csv, 'w')
        hm.plot(param_ranges, logx=options.logx, logy=options.logy,
                contour=options.contour_plot, csv=csv)
    finally:
        sr.teardown()
        if csv:
            csv.close()

# Copyright (C) 2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drudge's University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.


"""`Seminar` for running `sawsim` and parsing the results.
"""

from __future__ import with_statement

try:
    from collections import namedtuple
except ImportError:  # work around Python < 2.6
    from ._collections import namedtuple
from distutils.spawn import find_executable
import hashlib
from optparse import Option
import os
import os.path
from random import shuffle
import shutil
from uuid import uuid4

from . import __version__
from . import PYSAWSIM_LOG_LEVEL_MSG as _PYSAWSIM_LOG_LEVEL_MSG
from .manager import MANAGERS, get_manager, InvokeJob


_multiprocess_can_split_ = True
"""Allow nosetests to split tests between processes.
"""

SAWSIM = find_executable('sawsim')
if SAWSIM == None:
    SAWSIM = os.path.join('bin', 'sawsim')

CACHE_DIR = os.path.expanduser(os.path.join('~', '.sawsim-cache'))
DEFAULT_PARAM_STRING = (
    '-s cantilever,hooke,0.05 -N1 '
    '-s folded,null -N8 '
    "-s 'unfolded,wlc,{0.39e-9,28e-9}' "
    "-k 'folded,unfolded,bell,{3.3e-4,0.25e-9}' "
    '-q folded -v 1e-6')


# `Event` instances represent domain state transitions.
Event = namedtuple(
    typename='Event',
    field_names=['force', 'initial_state', 'final_state'])


class SawsimRunner (object):
    """
    >>> m = get_manager()()
    >>> sr = SawsimRunner(manager=m)
    >>> for run in sr(param_string=DEFAULT_PARAM_STRING, N=2):
    ...     print 'New run'
    ...     for i,event in enumerate(run):
    ...         print i, event  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
    New run
    0 Event(force=..., initial_state='folded', final_state='unfolded')
    1 Event(force=..., initial_state='folded', final_state='unfolded')
    2 Event(force=..., initial_state='folded', final_state='unfolded')
    3 Event(force=..., initial_state='folded', final_state='unfolded')
    4 Event(force=..., initial_state='folded', final_state='unfolded')
    5 Event(force=..., initial_state='folded', final_state='unfolded')
    6 Event(force=..., initial_state='folded', final_state='unfolded')
    7 Event(force=..., initial_state='folded', final_state='unfolded')
    New run
    0 Event(force=..., initial_state='folded', final_state='unfolded')
    1 Event(force=..., initial_state='folded', final_state='unfolded')
    2 Event(force=..., initial_state='folded', final_state='unfolded')
    3 Event(force=..., initial_state='folded', final_state='unfolded')
    4 Event(force=..., initial_state='folded', final_state='unfolded')
    5 Event(force=..., initial_state='folded', final_state='unfolded')
    6 Event(force=..., initial_state='folded', final_state='unfolded')
    7 Event(force=..., initial_state='folded', final_state='unfolded')
    >>> m.teardown()
    """

    optparse_options = [
        Option('-s','--sawsim', dest='sawsim',
               metavar='PATH',
               help='Set sawsim binary (%default).',
               default=SAWSIM),
        Option('-p','--params', dest='param_string',
               metavar='PARAMS',
               help='Initial params for fitting (%default).',
               default=DEFAULT_PARAM_STRING),
        Option('-N', '--number-of-runs', dest='N',
               metavar='INT', type='int',
               help='Number of sawsim runs at each point in parameter space (%default).',
               default=400),
        Option('-m', '--manager', dest='manager',
               metavar='STRING',
               help='Job manager name (one of %s) (default: auto-select).'
               % (', '.join(MANAGERS)),
               default=None),
        Option('-C','--use-cache', dest='use_cache',
               help='Use cached simulations if they exist (vs. running new simulations) (%default)',
               default=False, action='store_true'),
        Option('--clean-cache', dest='clean_cache',
               help='Remove previously cached simulations if they exist (%default)',
               default=False, action='store_true'),
        Option('-d','--cache-dir', dest='cache_dir',
               metavar='STRING',
               help='Cache directory for sawsim unfolding forces (%default).',
               default=CACHE_DIR),
    ]

    def __init__(self, sawsim=None, cache_dir=None,
                 use_cache=False, clean_cache=False,
                 manager=None):
        if sawsim == None:
            sawsim = SAWSIM
        self._sawsim = sawsim
        if cache_dir == None:
            cache_dir = CACHE_DIR
        self._cache_dir = cache_dir
        self._use_cache = use_cache
        self._clean_cache = clean_cache
        self._manager = manager
        self._local_manager = False
        self._headline = None

    def initialize_from_options(self, options):
        self._sawsim = options.sawsim
        self._cache_dir = options.cache_dir
        self._use_cache = options.use_cache
        self._clean_cache = options.clean_cache
        self._manager = get_manager(options.manager)()
        self._local_manager = True
        call_params = {}
        for param in ['param_string', 'N']:
            try:
                call_params[param] = getattr(options, param)
            except AttributeError:
                pass
        return call_params

    def teardown(self):
        if self._local_manager == True:
            self._manager.teardown()

    def __call__(self, param_string, N):
        """Run `N` simulations and yield `Event` generators for each run.

        Use the `JobManager` instance `manager` for asynchronous job
        execution.

        If `_use_cache` is `True`, store an array of unfolding forces
        in `cache_dir` for each simulated pull.  If the cached forces
        are already present for `param_string`, do not redo the
        simulation unless `_clean_cache` is `True`.
        """
        count = N
        if self._use_cache == True:
            d = self._param_cache_dir(param_string)
            if os.path.exists(d):
                if self._clean_cache == True:
                    shutil.rmtree(d)
                    self._make_cache(param_string)
                else:
                    for data in self._load_cached_data(param_string):
                        yield data
                        count -= 1
                        if count == 0:
                            return
            else:
                self._make_cache(param_string)

        jobs = {}
        for i in range(count):
            jobs[i] = self._manager.async_invoke(InvokeJob(
                    target='%s %s' % (self._sawsim, param_string)))
        complete_jobs = self._manager.wait(
            [job.id for job in jobs.itervalues()])
        for i,job in jobs.iteritems():
            j = complete_jobs[job.id]
            assert j.status == 0, j.data['error']
            if self._use_cache == True:
                self._cache_run(d, j.data['stdout'])
            yield self.parse(j.data['stdout'])
        del(jobs)
        del(complete_jobs)

    def _param_cache_dir(self, param_string):
        """
        >>> s = SawsimRunner()
        >>> s._param_cache_dir(DEFAULT_PARAM_STRING)  # doctest: +ELLIPSIS
        '/.../.sawsim-cache/...'
        """
        return os.path.join(
            self._cache_dir, hashlib.sha256(param_string).hexdigest())

    def _make_cache(self, param_string):
        cache_dir = self._param_cache_dir(param_string)
        os.makedirs(cache_dir)
        with open(os.path.join(cache_dir, 'param_string'), 'w') as f:
            f.write('# version: %s\n%s\n' % (__version__, param_string))

    def _load_cached_data(self, param_string):
        pcd = self._param_cache_dir(param_string)
        filenames = os.listdir(pcd)
        shuffle(filenames)
        for filename in filenames:
            if not filename.endswith('.dat'):
                continue
            with open(os.path.join(pcd, filename), 'r') as f:
                yield self.parse(f.read())

    def _cache_run(self, cache_dir, stdout):
        simulation_path = os.path.join(cache_dir, '%s.dat' % uuid4())
        with open(simulation_path, 'w') as f:
            f.write(stdout)

    def parse(self, text):
        """Parse the output of a `sawsim` run.
    
        >>> text = '''#Force (N)\\tinitial state\\tFinal state
        ... 2.90301e-10\\tfolded\\tunfolded
        ... 2.83948e-10\\tfolded\\tunfolded
        ... 2.83674e-10\\tfolded\\tunfolded
        ... 2.48384e-10\\tfolded\\tunfolded
        ... 2.43033e-10\\tfolded\\tunfolded
        ... 2.77589e-10\\tfolded\\tunfolded
        ... 2.85343e-10\\tfolded\\tunfolded
        ... 2.67796e-10\\tfolded\\tunfolded
        ... '''
        >>> sr = SawsimRunner()
        >>> events = list(sr.parse(text))
        >>> len(events)
        8
        >>> events[0]  # doctest: +ELLIPSIS
        Event(force=2.9030...e-10, initial_state='folded', final_state='unfolded')
        >>> sr._headline
        ['Force (N)', 'initial state', 'Final state']
        """
        for line in text.splitlines():
            line = line.strip()
            if len(line) == 0:
                continue
            elif line.startswith('#'):
                if self._headline == None:
                    self._headline = line[len('#'):].split('\t')
                continue
            fields = line.split('\t')
            if len(fields) != 3:
                raise ValueError(fields)
            force,initial_state,final_state = fields
            yield Event(float(force), initial_state, final_state)


def main(argv=None):
    """
    >>> try:
    ...     main(['--help'])
    ... except SystemExit, e:
    ...     pass  # doctest: +ELLIPSIS, +REPORT_UDIFF
    Usage: ... [options]
    <BLANKLINE>
    Options:
      -h, --help            show this help message and exit
      -s PATH, --sawsim=PATH
                            Set sawsim binary (...sawsim).
      ...
    >>> print e
    0
    >>> main(['-N', '2'])
    ... # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
    #Force (N)  Initial state  Final state
    ...         folded         unfolded
    ...         folded         unfolded
    ...         folded         unfolded
    ...         folded         unfolded
    ...
    ...         folded         unfolded
    """
    from optparse import OptionParser
    import sys

    if argv == None:
        argv = sys.argv[1:]

    sr = SawsimRunner()

    usage = '%prog [options]'
    epilog = '\n'.join([
            'Python wrapper around `sawsim`.  Distribute `N` runs using',
            'one of the possible job "managers".  Also supports caching',
            'results to speed future runs.',
            _PYSAWSIM_LOG_LEVEL_MSG,
            ])
    parser = OptionParser(usage, epilog=epilog)
    parser.format_epilog = lambda formatter: epilog+'\n'
    for option in sr.optparse_options:
        parser.add_option(option)
    
    options,args = parser.parse_args(argv)

    try:
        sr_call_params = sr.initialize_from_options(options)
    
        first_run = True
        for run in sr(**sr_call_params):
            if first_run == True:
                first_run = False
                run = list(run)  # force iterator evaluation
                if sr._headline != None:
                    print '#%s' % '\t'.join(sr._headline)
            for event in run:
                print '\t'.join([str(x) for x in event])
    finally:
        sr.teardown()

# Copyright (C) 2009-2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Functions for running external commands in subprocesses.
"""

from subprocess import Popen, PIPE
import sys


_multiprocess_can_split_ = True
"""Allow nosetests to split tests between processes.
"""


class CommandError(Exception):
    """Represent errors in command execution.

    Instances are picklable (for passing through `multiprocessing.Queue`\s).

    >>> import pickle
    >>> a = CommandError('somefunc', 1, '', 'could not find "somefunc"')
    >>> x = pickle.dumps(a)
    >>> b = pickle.loads(x)
    >>> print b
    Command failed (1):
      could not find "somefunc"
    <BLANKLINE>
    while executing
      somefunc
    >>> print repr(b)  # doctest: +NORMALIZE_WHITESPACE
    CommandError(command='somefunc', status=1, stdout='',
                 stderr='could not find "somefunc"')
    """
    def __init__(self, command=None, status=None, stdout=None, stderr=None):
        self.command = command
        self.status = status
        self.stdout = stdout
        self.stderr = stderr
        Exception.__init__(self, self.__str__())

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, data):
        self.__dict__.update(data)

    def __str__(self):
        return "\n".join([
                "Command failed (%s):\n  %s\n" % (self.status, self.stderr),
                "while executing\n  %s" % self.command,
                ])

    def __repr__(self):
        return '%s(%s)' % (
            self.__class__.__name__,
            ', '.join(['%s=%s' % (attr, repr(getattr(self, attr)))
                       for attr in ['command', 'status', 'stdout', 'stderr']]))


def invoke(cmd_string, stdin=None, expect=(0,), cwd=None, verbose=False):
    """
    expect should be a tuple of allowed exit codes.  cwd should be
    the directory from which the command will be executed.
    """
    if cwd == None:
        cwd = "."
    if verbose == True:
        print >> sys.stderr, "%s$ %s" % (cwd, cmd_string)
    try :
        if sys.platform != "win32" and False:
            q = Popen(cmd_string, stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd=cwd)
        else:
            # win32 don't have os.execvp() so have to run command in a shell
            q = Popen(cmd_string, stdin=PIPE, stdout=PIPE, stderr=PIPE,
                      shell=True, cwd=cwd)
    except OSError, e :
        raise CommandError(cmd_string, status=e.args[0], stdout="", stderr=e)
    stdout,stderr = q.communicate(input=stdin)
    status = q.wait()
    if verbose == True:
        print >> sys.stderr, "%d\n%s%s" % (status, stdout, stderr)
    if status not in expect:
        raise CommandError(cmd_string, status, stdout, stderr)
    return status, stdout, stderr

# Copyright (C) 2008-2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Test a Hookian chain with constant unfolding rate.

With the constant velocity experiment and a Hookian domain, the
unfolding force is proportional to time, so we expect exponential
population decay, even with multiple unfolding domains.

Analytically, with a spring constant

.. math:: k = df/dx

and a pulling velocity

.. math:: v = dx/dt

we have a loading rate

.. math:: df/dt = df/dx dx/dt = kv \\;,

so

.. math:: f = kvt  + f_0 \\;.

With an unfolding rate constant :math:`K`, the population follows

.. math::
  dp/dt = Kp
  p(t) = exp(-tK) = exp(-(f-f_0)K/kv) = p(f) \\;.

Therefore, a histogram of unfolding vs. force :math:`p(f)` normalized
to :math:`p(0)=1` should follow

.. math::
  -w N dp/dF = w N pK/kv = w N K/kv exp(-FK/kv)

where :math:`w` is the binwidth and :math:`N` is the number of tested
domains.

>>> test()
"""

from numpy import arange, exp, log

from ..histogram import Histogram
from ..fit import HistogramModelFitter
from ..manager import get_manager
from ..sawsim import SawsimRunner
from ..sawsim_histogram import sawsim_histogram


def probability_distribution(x, params):
    """Exponential decay decay probability distribution.

    .. math:: 1/\\tau \\cdot exp(-x/\\tau)
    """
    p = params  # convenient alias
    p[0] = abs(p[0])  # cannot normalize negative tau.
    return (1.0/p[0]) * exp(-x/p[0])


class ExponentialModelFitter (HistogramModelFitter):
    """Exponential decay model fitter.
    """
    def model(self, params):
        """An exponential decay model.

        Notes
        -----
        .. math:: y \\propto \\cdot e^{-t/\\tau}
        """
        self._model_data.counts = (
            self.info['binwidth']*self.info['N']*probability_distribution(
                self._model_data.bin_centers, params))
        return self._model_data

    def guess_initial_params(self):
        return [self._data.mean]

    def guess_scale(self, params):
        return [self._data.mean]

    def model_string(self):
        return 'p(x) = A exp(-t/tau)'

    def param_string(self, params):
        pstrings = []
        for name,param in zip(['tau'], params):
            pstrings.append('%s=%g' % (name, param))
        return ', '.join(pstrings)



def constant_rate(sawsim_runner, num_domains=1, unfolding_rate=1,
                  spring_constant=1, velocity=1, N=100):
    loading_rate = float(spring_constant * velocity)
    tau = loading_rate / unfolding_rate
    w = 0.2 * tau  # calculate bin width (in force)
    A = w*num_domains*N / tau
    theory = Histogram()
    # A exp(-x/tau) = 0.001
    #        -x/tau = log(0.001/A)
    #             x = -log(0.001/A)*tau = log(A/0.001)*tau = log(1e3*A)*tau
    theory.bin_edges = arange(start=0, stop=log(1e3*A)*tau, step=w)
    theory.bin_centers = theory.bin_edges[:-1] + w/2
    theory.counts = w*num_domains*N*probability_distribution(
        theory.bin_centers, [tau])
    theory.analyze()

    max_force_step = w/10.0
    max_time_step = max_force_step / loading_rate
    param_string = (
        '-d %(max_time_step)g -F %(max_force_step)g -v %(velocity)g '
        '-s cantilever,hooke,%(spring_constant)g -N1 '
        '-s folded,null -N%(num_domains)d -s unfolded,null '
        '-k folded,unfolded,const,%(unfolding_rate)g -q folded '
        ) % locals()
    sim = sawsim_histogram(sawsim_runner, param_string, N=N,
                           bin_edges=theory.bin_edges)
    
    e = ExponentialModelFitter(sim)
    params = e.fit()
    sim_tau = abs(params[0])
    for s,t,n in [(sim_tau, tau, 'tau')]:
        assert abs(s - t)/w < 3, (
            'simulation %s = %g != %g = %s (bin width = %g)' % (n,s,t,n,w))
    return sim.residual(theory)


def test(num_domains=[1,50], unfolding_rate=[1,2], spring_constant=[1,2],
         velocity=[1,2], threshold=0.2):
    m = get_manager()()
    sr = SawsimRunner(manager=m)
    try:
        for nd in num_domains:
            for ur in unfolding_rate:
                for sc in spring_constant:
                    for v in velocity:
                        residual = constant_rate(
                            sawsim_runner=sr, num_domains=nd,
                            unfolding_rate=ur, spring_constant=sc, velocity=v)
                        assert residual < threshold, residual
    finally:
        m.teardown()

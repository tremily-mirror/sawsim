# Copyright (C) 2009-2010  W. Trevor King <wking@drexel.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at <wking@drexel.edu> on the Internet, or
# write to Trevor King, Drexel University, Physics Dept., 3141 Chestnut St.,
# Philadelphia PA 19104, USA.

"""Histogram generation and comparison.
"""

import matplotlib
matplotlib.use('Agg')  # select backend that doesn't require X Windows
import numpy
import pylab

from . import log


_multiprocess_can_split_ = True
"""Allow nosetests to split tests between processes.
"""

FIGURE = pylab.figure()  # avoid memory problems.
"""`pylab` keeps internal references to all created figures, so share
a single instance.
"""


class Histogram (object):
    """A histogram with a flexible comparison method, `residual()`.

    >>> h = Histogram()
    """
    def __init__(self):
        self.headings = None

    def calculate_bin_edges(self, data, bin_width):
        """
        >>> h = Histogram()
        >>> h.calculate_bin_edges(numpy.array([-7.5, 18.2, 4]), 10)
        array([-10.,   0.,  10.,  20.])
        >>> h.calculate_bin_edges(numpy.array([-7.5, 18.2, 4, 20]), 10)
        array([-10.,   0.,  10.,  20.])
        >>> h.calculate_bin_edges(numpy.array([0, 18.2, 4, 20]), 10)
        array([  0.,  10.,  20.])
        >>> h.calculate_bin_edges(numpy.array([18.2, 4, 20]), 10)
        array([  0.,  10.,  20.])
        >>> h.calculate_bin_edges(numpy.array([18.2, 20]), 10)
        array([ 10.,  20.])
        """
        m = data.min()
        M = data.max()
        bin_start = m - m % bin_width
        return numpy.arange(bin_start, M+bin_width, bin_width, dtype=data.dtype)

    def from_data(self, data, bin_edges):
        """Initialize from `data`.

        All bins should be of equal width (so we can calculate which
        bin a data point belongs to).
        """
        data = numpy.array(data)
        self.bin_edges = numpy.array(bin_edges)
        bin_width = self.bin_edges[1] - self.bin_edges[0]

        bin_is = numpy.floor((data - self.bin_edges[0])/bin_width)
        self.counts = numpy.zeros((len(self.bin_edges)-1,), dtype=numpy.int)
        for i in range(len(self.counts)):
            self.counts[i] = (bin_is == i).sum()
        self.counts = numpy.array(self.counts)
        self.total = float(len(data)) # some data might be outside the bins
        self.mean = data.mean()
        self.std_dev = data.std()
        self.normalize()

    def from_stream(self, stream):
        """Initialize from `stream`.

        File format:

            # comment and blank lines ignored
            <bin_edge><whitespace><count>
            ...

        `<bin_edge>` should mark the left-hand side of the bin, and
        all bins should be of equal width (so we know where the last
        one ends).

        >>> import StringIO
        >>> h = Histogram()
        >>> h.from_stream(StringIO.StringIO('''# Force (N)\\tUnfolding events
        ... 150e-12\\t10
        ... 200e-12\\t40
        ... 250e-12\\t5
        ... '''))
        >>> h.headings
        ['Force (N)', 'Unfolding events']
        >>> h.total
        55.0
        >>> h.counts
        [10.0, 40.0, 5.0]
        >>> h.bin_edges  # doctest: +ELLIPSIS
        [1.5e-10, 2...e-10, 2.5...e-10, 3e-10]
        >>> h.probabilities  # doctest: +ELLIPSIS
        [0.181..., 0.727..., 0.0909...]
        """
        self.headings = None
        self.bin_edges = []
        self.counts = []
        for line in stream.readlines():
            line = line.strip()
            if len(line) == 0 or line.startswith('#'):
                if self.headings == None and line.startswith('#'):
                    line = line[len('#'):]
                    self.headings = [x.strip() for x in line.split('\t')]
                continue # ignore blank lines and comments
            try:
                bin_edge,count = line.split()
            except ValueError:
                log().error('Unable to parse histogram line: "%s"' % line)
                raise
            self.bin_edges.append(float(bin_edge))
            self.counts.append(float(count))
        bin_width = self.bin_edges[1] - self.bin_edges[0]
        self.bin_edges.append(self.bin_edges[-1]+bin_width)
        self.analyze()

    def to_stream(self, stream):
        """Write to `stream` with the same format as `from_stream()`.

        >>> import sys
        >>> h = Histogram()
        >>> h.headings = ['Force (N)', 'Unfolding events']
        >>> h.bin_edges = [1.5e-10, 2e-10, 2.5e-10, 3e-10]
        >>> h.counts = [10, 40, 5]
        >>> h.to_stream(sys.stdout)
        ... # doctest: +NORMALIZE_WHITESPACE, +REPORT_UDIFF
        #Force (N)\tUnfolding events
        1.5e-10\t10
        2e-10\t40
        2.5e-10\t5
        >>> h.bin_edges = h.bin_edges[:2]
        >>> h.counts = h.counts[:1]
        >>> h.to_stream(sys.stdout)
        ... # doctest: +NORMALIZE_WHITESPACE, +REPORT_UDIFF
        #Force (N)\tUnfolding events
        1.5e-10\t10
        2e-10\t0
        """
        if self.headings != None:
            stream.write('#%s\n' % '\t'.join(self.headings))
        for bin,count in zip(self.bin_edges, self.counts):
            stream.write('%g\t%g\n' % (bin, count))
        if len(self.counts) == 1:
            # print an extra line so that readers can determine bin width
            stream.write('%g\t0\n' % (self.bin_edges[-1]))

    def analyze(self):
        """Analyze `.counts` and `.bin_edges` if the raw data is missing.

        Generate `.total`, `.mean`, and `.std_dev`, and run
        `.normalize()`.
        """
        bin_width = self.bin_edges[1] - self.bin_edges[0]
        self.total = float(sum(self.counts))
        self.mean = 0
        for bin,count in zip(self.bin_edges, self.counts):
            bin += bin_width / 2.0
            self.mean += bin * count
        self.mean /=  float(self.total)
        variance = 0
        for bin,count in zip(self.bin_edges, self.counts):
            bin += bin_width / 2.0
            variance += (bin - self.mean)**2 * count
        self.std_dev = numpy.sqrt(variance)
        self.normalize()

    def normalize(self):
        """Generate `.probabilities` from `.total` and `.counts`.
        """
        self.total = float(self.total)
        self.probabilities = [count/self.total for count in self.counts]

    def mean_residual(self, other):
        return abs(other.mean - self.mean)

    def std_dev_residual(self, other):
        return abs(other.std_dev - self.std_dev)

    def chi_squared_residual(self, other):
        assert (self.bin_edges == other.bin_edges).all()
        residual = 0
        for probA,probB in zip(self.probabilities, other.probabilities):
            residual += (probA-probB)**2 / probB
        return residual

    def jensen_shannon_residual(self, other):
        assert (self.bin_edges == other.bin_edges).all()
        def d_KL_term(p,q):
            """
            Kullback-Leibler divergence for a single bin, with the
            exception that we return 0 if q==0, rather than
            exploding like d_KL should.  We can do this because
            the way d_KL_term is used in the Jensen-Shannon
            divergence, if q (really m) == 0, then p also == 0,
            and the Jensen-Shannon divergence for the entire term
            should be zero.
            """
            if p==0 or q==0 or p==q:
                return 0.0
            return p * numpy.log2(p/q)
        residual = 0
        for probA,probB in zip(self.probabilities, other.probabilities):
            m = (probA+probB) / 2.0
            residual += 0.5*(d_KL_term(probA,m) + d_KL_term(probB,m))
        return residual

    def _method_to_type(self, name):
        return name[:-len('_residual')].replace('_', '-')

    def _type_to_method(self, name):
        return '%s_residual' % name.replace('-', '_')

    def types(self):
        """Return a list of supported residual types.
        """
        return sorted([self._method_to_type(x)
                       for x in dir(self) if x.endswith('_residual')])

    def residual(self, other, type='jensen-shannon'):
        """Compare this histogram with `other`.

        Supported comparison `type`\s may be found with `types()`:

        >>> h = Histogram()
        >>> h.types()
        ['chi-squared', 'jensen-shannon', 'mean', 'std-dev']

        Selecting an invalid `type` raises an exception.

        >>> h.residual(other=None, type='invalid-type')
        Traceback (most recent call last):
          ...
        AttributeError: 'Histogram' object has no attribute 'invalid_type_residual'

        For residual types where there is a convention, this histogram
        is treated as the experimental histogram and the other
        histogram is treated as the theoretical one.
        """
        r_method = getattr(self, self._type_to_method(type))
        return r_method(other)

    def plot(self, title=None, filename=None):
        FIGURE.clear()
        axes = FIGURE.add_subplot(1, 1, 1)
        axes.hist(x=self.bin_edges[:-1],  # one fake entry for each bin
                  weights=self.counts,    # weigh the fake entries by count
                  bins=self.bin_edges,
                  align='mid', histtype='stepfilled')
        axes.set_title(title)
        pylab.show()
        FIGURE.savefig(filename)

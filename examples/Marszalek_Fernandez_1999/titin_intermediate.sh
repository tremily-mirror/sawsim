#!/bin/bash

# rediculous parameters, but it vaguely resembles the Titin shoulder
# from Marszalek...Fernandez 1999, "Mechanical unfolding intermediates
# in titin modules.", Nature (402), 6757 p100.

sawsim -v1e-6 \
  -s cantilever,hooke,0.05 -N1 \
  -s folded,null -N8 \
  -s 'transition,wlc,{0.39e-9,1e-9}' \
  -s 'unfolded,wlc,{0.39e-9,28e-9}' \
  -s 'cantbind,null' -N1 \
  -k 'folded,transition,bell,{1e-12,1e-9}' \
  -k 'transition,folded,bell,{1e8,-1e-9}' \
  -k 'transition,unfolded,bell,{3.3e-4,0.25e-9}' \
  -k 'cantbind,unfolded,bell,{3.3e-6,0.25e-9}' \
  -q cantbind -V

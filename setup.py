#!/usr/bin/env python

"Python framework for the sawsim force spectroscopy simulator."

from distutils.core import setup
from os import listdir
import os.path

from pysawsim import __version__


package_name = 'sawsim'
python_package_name = 'pysawsim'

SCRIPT_DIR = 'bin'
scripts = [os.path.join(SCRIPT_DIR, s) for s in listdir(SCRIPT_DIR)
           if s.endswith('.py')]
packages = [
    'pysawsim',
    'pysawsim.manager',
    'pysawsim.test']

_this_dir = os.path.dirname(__file__)


setup(
    name=python_package_name,
    version=__version__,
    author='W. Trevor King',
    author_email='wking@tremily.us',
    description=__doc__,
    long_description = open(os.path.join(_this_dir, 'README'), 'r').read(),
    url='http://blog.tremily.us/posts/{}/'.format(package_name),
    download_url='http://git.tremily.us/?p={}.git;a=snapshot;h=v{};sf=tgz'.format(
        package_name, __version__),
    packages=packages,
    provides=packages,
    scripts=scripts,
    )
